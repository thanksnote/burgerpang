"""
urls.py

URL dispatch route mappings and error handlers

"""
from flask import render_template

from application import app
from application import views, pad_views


## URL dispatch rules
# App Engine warm up handler
# See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests
app.add_url_rule('/_ah/warmup', 'warmup', view_func=views.warmup)

# Home page
app.add_url_rule('/', 'home', view_func=views.home)

# Say hello
app.add_url_rule('/hello/<username>', 'say_hello', view_func=views.say_hello)

# Examples list page
app.add_url_rule('/examples', 'list_examples', view_func=views.list_examples, methods=['GET', 'POST'])

# Examples list page (cached)
app.add_url_rule('/examples/cached', 'cached_examples', view_func=views.cached_examples, methods=['GET'])

# Contrived admin-only view example
app.add_url_rule('/admin_only', 'admin_only', view_func=views.admin_only)

# Edit an example
app.add_url_rule('/examples/<int:example_id>/edit', 'edit_example', view_func=views.edit_example,
                 methods=['GET', 'POST'])

# Delete an example
app.add_url_rule('/examples/<int:example_id>/delete', view_func=views.delete_example, methods=['POST'])

## BurgerPang
app.add_url_rule('/user/available/<string:id>', view_func=views.available, methods=['GET'])
app.add_url_rule('/user/add', view_func=views.addUser, methods=['POST'])
app.add_url_rule('/user/<string:id>', view_func=views.getUser, methods=['GET'])
app.add_url_rule('/user/<string:id>/score', view_func=views.addScore, methods=['POST'])
app.add_url_rule('/user/<string:id>/character/insert', view_func=views.addChracter, methods=['POST'])

app.add_url_rule('/user/<string:id>/burger', view_func=views.updateBurger, methods=['POST'])
app.add_url_rule('/user/<string:id>/extra_burger', view_func=views.updateExtraBurger, methods=['POST'])
app.add_url_rule('/user/<string:id>/name', view_func=views.updateName, methods=['POST'])
app.add_url_rule('/user/<string:id>/region', view_func=views.updateRegion, methods=['POST'])
app.add_url_rule('/user/<string:id>/gold', view_func=views.updateGold, methods=['POST'])
app.add_url_rule('/user/<string:id>/character', view_func=views.updateCharacter, methods=['POST'])

app.add_url_rule('/user/<string:id>/rank/<int:week>', view_func=views.getUserRank, methods=['GET'])
app.add_url_rule('/rank/<int:region>/<int:week>', view_func=views.getWeekTopRanksByRegion, methods=['GET'])
app.add_url_rule('/period/current', view_func=views.getGameInfo, methods=['GET'])

app.add_url_rule('/purchase/gold', view_func=views.getPurchaseGoldInfo, methods=['GET'])
app.add_url_rule('/purchase/burger', view_func=views.getPurchaseBurgerInfo, methods=['GET'])
app.add_url_rule('/purchase/gold/event', view_func=views.getEventInfo, methods=['GET'])
app.add_url_rule('/purchase/burger/event', view_func=views.getBurgerEventInfo, methods=['GET'])

# PaD
app.add_url_rule('/pad/schedule', view_func=pad_views.schedule, methods=['GET'])


## Error handlers
# Handle 404 errors
@app.errorhandler(404)
def page_not_found(e):
  return render_template('404.html'), 404

# Handle 500 errors
@app.errorhandler(500)
def server_error(e):
  return render_template('500.html'), 500

