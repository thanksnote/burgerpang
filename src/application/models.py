"""
models.py

App Engine datastore models

"""

from google.appengine.ext import ndb

region = {
  "su": 1,
  "ic": 2,
  "tj": 3,
  "kj": 4,
  "tg": 5,
  "us": 6,
  "ps": 7,
  "kg": 8,
  "cb": 9,
  "cn": 10,
  "jb": 11,
  "jn": 12,
  "kw": 13,
  "kb": 14,
  "kn": 15,
  "cj": 16
}

character = {
  "doojin": 1,
  "sunghee": 2,
  "goodoi": 3,
  "jeongseok": 4
}


class ExampleModel(ndb.Model):
  """Example Model"""
  example_name = ndb.StringProperty(required=True)
  example_description = ndb.TextProperty(required=True)
  added_by = ndb.UserProperty()
  timestamp = ndb.DateTimeProperty(auto_now_add=True)


class User(ndb.Model):
  name = ndb.StringProperty(required=True)
  region = ndb.IntegerProperty(required=True)
  gold = ndb.IntegerProperty(default=0)
  burger = ndb.IntegerProperty(default=5)
  extra_burger = ndb.IntegerProperty(default=0)
  character = ndb.IntegerProperty(default=1)
  character_list = ndb.IntegerProperty(repeated=True)
  modified = ndb.DateTimeProperty(required=True, auto_now=True)


class Score(ndb.Model):
  user = ndb.KeyProperty(required=True)
  name = name = ndb.StringProperty(required=True)
  character = ndb.IntegerProperty(default=1)
  score = ndb.IntegerProperty(required=True)
  year = ndb.IntegerProperty(required=True)
  week = ndb.IntegerProperty(required=True)
  region = ndb.IntegerProperty(required=True)
  created = ndb.DateTimeProperty(auto_now_add=True)

class Rank(ndb.Model):
  name = name = ndb.StringProperty(required=True)
  character = ndb.IntegerProperty(default=1)
  score = ndb.IntegerProperty(required=True)
  year = ndb.IntegerProperty(required=True)
  week = ndb.IntegerProperty(required=True)
  region = ndb.IntegerProperty(required=True)
  created = ndb.DateTimeProperty(auto_now_add=True)