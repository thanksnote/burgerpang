# -*- coding: utf-8 -*-
__author__ = 'thanksnote'

import datetime


class UtcTzinfo(datetime.tzinfo):
	def utcoffset(self, dt):
		return datetime.timedelta(0)

	def dst(self, dt):
		return datetime.timedelta(0)

	def tzname(self, dt):
		return 'UTC'

	def olsen_name(self):
		return 'UTC'