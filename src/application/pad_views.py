"""
views.py

URL route handlers

Note that any handler params must match the URL route params.
For example the *say_hello* handler, handling the URL route '/hello/<username>',
  must be passed *username* as the argument.

"""
from google.appengine.api import users
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError

from flask import request, render_template, flash, url_for, redirect, jsonify
from flask_cache import Cache

from application import app
from dateutil import tz
import time
from datetime import datetime
from timezone import UtcTzinfo

from pad.crawler import *
from cron.pad_notify import *


# Flask-Cache (configured to use App Engine Memcache API)
cache = Cache(app)

SIMPLE_TYPES = (int, long, float, bool, dict, basestring, list)


def to_dict(model):
  output = { }
  output['id'] = model.key.urlsafe()
  for key, prop in model._properties.iteritems():
    value = getattr(model, key)

    if value is None or isinstance(value, SIMPLE_TYPES):
      output[key] = value
    elif isinstance(value, datetime):
      to_zone = tz.gettz('Asia/Seoul')
      value = value.replace(tzinfo=UtcTzinfo()).astimezone(to_zone)
      ms = time.mktime(value.utctimetuple())
      ms += getattr(value, 'microseconds', 0) / 1000
      output[key] = int(ms)
    elif isinstance(value, ndb.GeoPt):
      output[key] = { 'lat': value.lat, 'lon': value.lon }
    #        elif isinstance(value, ndb.Model):
    #            output[key] = to_dict(value)
    elif isinstance(value, ndb.Key):
      output[key] = value.urlsafe()
    else:
      raise ValueError('cannot encode ' + repr(prop))

  return output

def schedule():
    jsonData = getPADSchedule()
    return jsonData
    # if jsonData is not None:
    #     # sendAPNServer(jsonData)

if __name__ == '__main__':
    schedule()