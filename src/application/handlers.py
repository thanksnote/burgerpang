# -*- coding:utf-8 -
__author__ = 'thanksnote'

from models import *

import time
from datetime import datetime


class UserHandler():
  @classmethod
  def add(cls, name, region):
    key = User(name=name, region=region, character_list=[1]).put()
    return key.urlsafe()

  @classmethod
  def available(cls, id):
    user = ndb.Key(urlsafe=id).get()
    return user is None

  @classmethod
  def get(cls, id):
    return ndb.Key(urlsafe=id).get()

  @classmethod
  def updateBurgerCount(cls, id, operation, count):
    user = ndb.Key(urlsafe=id).get()
    if operation == "decrease":
      count = count * -1
    elif operation == "increase":
      # todo nothing
      pass
    else:
      count = 0

    user.burger = user.burger + count
    user.put()
    return True

  @classmethod
  def updateExtraBurgerCount(cls, id, operation, count):
    user = ndb.Key(urlsafe=id).get()
    if operation == "decrease":
      count = count * -1
    elif operation == "increase":
      # todo nothing
      pass
    else:
      count = 0

    user.extra_burger = user.extra_burger + count
    user.put()
    return True

  @classmethod
  def updateName(cls, id, name):
    user = ndb.Key(urlsafe=id).get()
    user.name = name
    user.put()
    return True

  @classmethod
  def updateGold(cls, id, gold):
    user = ndb.Key(urlsafe=id).get()
    user.gold = gold
    user.put()
    return True

  @classmethod
  def updateRegion(cls, id, region):
    user = ndb.Key(urlsafe=id).get()
    user.region = region
    user.put()
    return True

  @classmethod
  def updateCharacter(cls, id, character):
    user = ndb.Key(urlsafe=id).get()
    user.character = character
    user.put()
    return True

  @classmethod
  @ndb.transactional
  def addCharacter(cls, id, newChracter):
    user = cls.get(id)
    user.character_list.append(newChracter)
    res = user.put()
    if res != None:
      return True
    else:
      return False


class ScoreHandler():

  @classmethod
  @ndb.transactional(xg=True)
  def add(cls, id, year, week, score):
    userKey = ndb.Key(urlsafe=id)
    user = userKey.get()
    inputScore = Score(user=userKey, name=user.name, character=user.character, year=year, week=week, score=score, region=user.region).put()


    rankScores = Rank.query(ancestor = userKey).filter(Rank.year == year, Rank.week == week, Rank.region == user.region).fetch()
    if rankScores:
      rank = rankScores[0]
      if rank.score < score:
        rank.score = score
        rankKey = rank.put()
        rankKey.get()
    else:
      rank = Rank(parent=userKey)
      rank.name = user.name
      rank.character = user.character
      rank.score = score
      rank.year = year
      rank.week = week
      rank.region = user.region
      rank.put()

    return inputScore != None

  @classmethod
  def getBestScore(cls, id, week):
    userKey = ndb.Key(urlsafe=id)
    user = userKey.get()
    scores = [rank.score for rank in Rank.query(ancestor = userKey).filter(Rank.week == week, Rank.region == user.region).fetch()]
    if 0 == len(scores):
      return 0
    return max(scores)

  @classmethod
  def getRank(cls, id, year, week):
    userKey = ndb.Key(urlsafe=id)
    user = userKey.get()
    bestScore = ScoreHandler.getBestScore(id, week)
    totalRank = Rank.query(Rank.year == year, Rank.week == week, Rank.score > bestScore).count() + 1
    localRank = Rank.query(Rank.year == year, Rank.week == week, Rank.region == user.region, Rank.score > bestScore).count() + 1
    return {
      "best_score": bestScore,
      "rank": {
        "all": totalRank,
        "local": localRank
      }
    }

  @classmethod
  def getWeekTopRanksByRegion(cls, region, year, week):
    # topScores = Score.query(Score.week == week, Score.region == region).order(-Score.score).fetch(20)
    # return [{ "user_name": score.name, "best_score": score.score, "character": score.character } for score in topScores]topScores = Score.query(Score.week == week, Score.region == region).order(-Score.score).fetch(20)
    topRanks = Rank.query(Rank.year == year, Rank.week == week, Rank.region == region).order(-Rank.score).fetch(20)
    return [{ "user_name": rank.name, "best_score": rank.score, "character": rank.character } for rank in topRanks]

