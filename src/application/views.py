"""
views.py

URL route handlers

Note that any handler params must match the URL route params.
For example the *say_hello* handler, handling the URL route '/hello/<username>',
  must be passed *username* as the argument.

"""
from google.appengine.api import users
from google.appengine.runtime.apiproxy_errors import CapabilityDisabledError

from flask import request, render_template, flash, url_for, redirect, jsonify
from flask_cache import Cache

from application import app
from decorators import login_required, admin_required
from forms import ExampleForm
from handlers import *
from dateutil import tz
import time
from datetime import datetime
from timezone import UtcTzinfo




# Flask-Cache (configured to use App Engine Memcache API)
cache = Cache(app)

SIMPLE_TYPES = (int, long, float, bool, dict, basestring, list)


def to_dict(model):
  output = { }
  output['id'] = model.key.urlsafe()
  for key, prop in model._properties.iteritems():
    value = getattr(model, key)

    if value is None or isinstance(value, SIMPLE_TYPES):
      output[key] = value
    elif isinstance(value, datetime):
      to_zone = tz.gettz('Asia/Seoul')
      value = value.replace(tzinfo=UtcTzinfo()).astimezone(to_zone)
      ms = time.mktime(value.utctimetuple())
      ms += getattr(value, 'microseconds', 0) / 1000
      output[key] = int(ms)
    elif isinstance(value, ndb.GeoPt):
      output[key] = { 'lat': value.lat, 'lon': value.lon }
    #        elif isinstance(value, ndb.Model):
    #            output[key] = to_dict(value)
    elif isinstance(value, ndb.Key):
      output[key] = value.urlsafe()
    else:
      raise ValueError('cannot encode ' + repr(prop))

  return output


def home():
  return redirect(url_for('list_examples'))


def say_hello(username):
  """Contrived example to demonstrate Flask's url routing capabilities"""
  return 'Hello %s' % username


@login_required
def list_examples():
  """List all examples"""
  examples = ExampleModel.query()
  form = ExampleForm()
  if form.validate_on_submit():
    example = ExampleModel(
      example_name=form.example_name.data,
      example_description=form.example_description.data,
      added_by=users.get_current_user()
    )
    try:
      example.put()
      example_id = example.key.id()
      flash(u'Example %s successfully saved.' % example_id, 'success')
      return redirect(url_for('list_examples'))
    except CapabilityDisabledError:
      flash(u'App Engine Datastore is currently in read-only mode.', 'info')
      return redirect(url_for('list_examples'))
  return render_template('list_examples.html', examples=examples, form=form)


@login_required
def edit_example(example_id):
  example = ExampleModel.get_by_id(example_id)
  form = ExampleForm(obj=example)
  if request.method == "POST":
    if form.validate_on_submit():
      example.example_name = form.data.get('example_name')
      example.example_description = form.data.get('example_description')
      example.put()
      flash(u'Example %s successfully saved.' % example_id, 'success')
      return redirect(url_for('list_examples'))
  return render_template('edit_example.html', example=example, form=form)


@login_required
def delete_example(example_id):
  """Delete an example object"""
  example = ExampleModel.get_by_id(example_id)
  try:
    example.key.delete()
    flash(u'Example %s successfully deleted.' % example_id, 'success')
    return redirect(url_for('list_examples'))
  except CapabilityDisabledError:
    flash(u'App Engine Datastore is currently in read-only mode.', 'info')
    return redirect(url_for('list_examples'))


@admin_required
def admin_only():
  """This view requires an admin account"""
  return 'Super-seekrit admin page.'


@cache.cached(timeout=60)
def cached_examples():
  """This view should be cached for 60 sec"""
  examples = ExampleModel.query()
  return render_template('list_examples_cached.html', examples=examples)


def warmup():
  """App Engine warmup handler
  See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests

  """
  return ''


def available(id):
  return jsonify({ "available": UserHandler.available(id) })


def addUser():
  name = request.json.get("name")
  region = request.json.get("region")
  return jsonify({ "id": UserHandler.add(name, region), "status": "success" })


def getUser(id):
  user = UserHandler.get(id)
  return jsonify(to_dict(user))

#@ndb.transactional(xg=True)
def addScore(id):
  ## request.json.get("id")
  dt = datetime.now().isocalendar()
  year = dt[0]
  week = dt[1]
  score = request.json.get("score")
  result = ScoreHandler.add(id, year, week, score)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })

def addChracter(id):
  newChracter = request.json.get("character")
  result = UserHandler.addCharacter(id, newChracter)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })


def updateBurger(id):
  operation = request.json.get("operation")
  count = request.json.get("count")
  result = UserHandler.updateBurgerCount(id, operation, count)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })

def updateExtraBurger(id):
  operation = request.json.get("operation")
  count = request.json.get("count")
  result = UserHandler.updateExtraBurgerCount(id, operation, count)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })


def getUserRank(id, week):
  to_zone = tz.gettz('Asia/Seoul')
  dt = datetime.now().replace(tzinfo=UtcTzinfo()).astimezone(to_zone)
  year = dt.isocalendar()[0]
  thisWeek = dt.isocalendar()[1] - week
  try:
    return jsonify(ScoreHandler.getRank(id, year, thisWeek))
  except:
    return jsonify({"status":"fail"})


def getWeekTopRanksByRegion(region, week):
  to_zone = tz.gettz('Asia/Seoul')
  dt = datetime.now().replace(tzinfo=UtcTzinfo()).astimezone(to_zone)
  year = dt.isocalendar()[0]
  thisWeek = dt.isocalendar()[1] - week
  ranks = ScoreHandler.getWeekTopRanksByRegion(region, year, thisWeek)
  return jsonify(rank=ranks)

def next_weekday(d, weekday):
    import datetime
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return d + datetime.timedelta(days_ahead)

def prev_weekday(d, weekday):
    import datetime
    days_before = weekday - d.weekday()
    if days_before > 0: # Target day already happened this week
        days_before -= 7
    return d + datetime.timedelta(days_before)


def getGameInfo():
    to_zone = tz.gettz('Asia/Seoul')
    d = datetime.now().replace(tzinfo=UtcTzinfo()).astimezone(to_zone)
    next_sunday = next_weekday(d, 6)
    print next_sunday
    last_monday = prev_weekday(d, 0)
    return jsonify({"current_date":d.strftime("%Y%m%d"),"start_date":last_monday.strftime("%Y%m%d"), "end_date":next_sunday.strftime("%Y%m%d")})


def updateName(id):
  name = request.json.get("name")
  result = UserHandler.updateName(id, name)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })

def updateRegion(id):
  region = request.json.get("region")
  result = UserHandler.updateRegion(id, region)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })


def updateGold(id):
  gold = request.json.get("gold")
  result = UserHandler.updateGold(id, gold)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })


def updateCharacter(id):
  character = request.json.get("character")
  result = UserHandler.updateCharacter(id, character)
  if result:
    status = "success"
  else:
    status = "fail"
  return jsonify({ "status": status })


def getPurchaseGoldInfo():
  golds  = [3,    15,   65,   180,  400]
  monies = [0.99, 1.99, 4.99, 9.99, 18.99]
  data = [{"gold":item[0], "money":item[1]} for item in zip(golds, monies)]
  return jsonify(purchase_info=data)


def getPurchaseBurgerInfo():
  burgerCounts = [3, 15, 65, 180, 400]
  golds = [5, 10, 40, 100, 200]
  data = [{"burgerCounts":item[0], "gold":item[1]} for item in zip(burgerCounts, golds)]
  return jsonify(purchase_info=data)


def getEventInfo():
  return jsonify({
    "event_url":"http://...",
    "purchase":{
      "gold":3,
      "money":0.99
    }
  })

def getBurgerEventInfo():
  return jsonify({
    "event_url":"http://...",
    "purchase":{
      "burgerCount":3,
      "gold":5
    }
  })