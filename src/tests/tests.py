#!/usr/bin/env python
# encoding: utf-8
"""
tests.py

TODO: These tests need to be updated to support the Python 2.7 runtime

"""
import unittest
import json

from google.appengine.ext import testbed

from application import app
from google.appengine.datastore import datastore_stub_util


class UserTestCase(unittest.TestCase):

  def setUp(self):
    # Flask apps testing. See: http://flask.pocoo.org/docs/testing/
    app.config['TESTING'] = True
    app.config['CSRF_ENABLED'] = False
    self.app = app.test_client()
    # Setups app engine test bed. See: http://code.google.com/appengine/docs/python/tools/localunittesting.html#Introducing_the_Python_Testing_Utilities
    self.testbed = testbed.Testbed()
    self.testbed.activate()
    self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(probability=0)
    self.testbed.init_datastore_v3_stub(consistency_policy=self.policy)
    self.testbed.init_user_stub()
    self.testbed.init_memcache_stub()

  def tearDown(self):
    self.testbed.deactivate()

  def test_addUser(self):
    jsonData = json.dumps({ 'name': 'dummyUserName', 'region': 1 })
    rv = self.app.post('/user/add', data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    assert "success" == res["status"]

  def test_getUser(self):
    ## fist add user
    jsonData = json.dumps({ 'name': 'dummyUserName', 'region': 1 })
    rv = self.app.post('/user/add', data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    assert "success" == res["status"]

    ## test get user
    id = res["id"]
    rv = self.app.get('/user/%s' % id)
    res = json.loads(rv.data)
    assert res["name"] == "dummyUserName"
    assert res["region"] == 1


  def addUser(self, user, region ):
    jsonData = json.dumps({ 'name': user, 'region': region})
    rv = self.app.post('/user/add', data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    return res

  def addScore(self, id, score):
    param = {}
    param["id"] = id
    param["score"] = score
    jsonData = json.dumps(param)
    rv = self.app.post('/user/%s/score' % id, data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    return "success" == res["status"]

  def test_addScore(self):
    res = self.addUser("dummyUserName", 1)
    self.assertEqual("success", res["status"])
    self.assertTrue(self.addScore(res["id"], 5000))

  def test_updateBurgerCount(self):
    jsonData = json.dumps({ 'name': 'dummyUserName', 'region': 1 })
    rv = self.app.post('/user/add', data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    assert "success" == res["status"]

    id = res["id"]
    param = {}
    param["id"] = id
    param["operation"] = "increase"
    param["count"] = 3
    jsonData = json.dumps(param)
    rv = self.app.post('/user/%s/burger' % id, data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    assert "success" == res["status"]

  def test_get_user_rank(self):
    """
    :request:
      url:  /user/<id:string>/rank/<week:int>
      method : GET

    :response:
      {
        best_score : <금주 최고 점수:int>
        rank :
        {
          all:1,
          local:2
        }
      }

    """
    # 서울에 3 명의 유저 생성
    region_code = 1
    user1 = self.addUser("user1", region_code)["id"]
    user2 = self.addUser("user2", region_code)["id"]
    user3 = self.addUser("user3", region_code)["id"]

    self.assertIsNotNone(user1)
    self.assertIsNotNone(user2)
    self.assertIsNotNone(user3)

    self.assertTrue(self.addScore(user1, 300))
    self.assertTrue(self.addScore(user2, 200))
    self.assertTrue(self.addScore(user3, 100))

    week_num = 0 ## this week
    rv = self.app.get('/user/%s/rank/%d' % (user1, week_num), data=None, content_type='application/json')
    res = json.loads(rv.data)
    self.assertEqual(300, res["best_score"])
    self.assertEqual(1, res["rank"]["all"])
    self.assertEqual(1, res["rank"]["local"])

  def test_get_purchase_gold(self):
    rv = self.app.get('/purchase/gold', data=None, content_type='application/json')
    res = json.loads(rv.data)
    self.assertEqual(res["purchase_info"][0]["money"], 0.99)
    self.assertEqual(res["purchase_info"][0]["gold"], 3)

  def test_get_purchase_burger(self):
    rv = self.app.get('/purchase/burger', data=None, content_type='application/json')
    res = json.loads(rv.data)
    self.assertEqual(res["purchase_info"][0]["burgerCounts"], 3)
    self.assertEqual(res["purchase_info"][0]["gold"], 5)

  def test_get_event_info(self):
    rv = self.app.get('/purchase/gold/event', data=None, content_type='application/json')
    res = json.loads(rv.data)
    self.assertEqual(0.99, res["purchase"]["money"])
    self.assertEqual(3, res["purchase"]["gold"])

  def test_add_character(self):

    # 서울에 1명의 사용자 생성
    region_code = 1
    user1 = self.addUser("user1", region_code)["id"]

    param = {}
    param["id"] = user1
    param["character"] = 99
    jsonData = json.dumps(param)
    rv = self.app.post('/user/%s/character/insert' % user1, data=jsonData, content_type='application/json')
    res = json.loads(rv.data)
    self.assertEqual("success", res["status"])

    rv = self.app.get('/user/%s' % user1)
    res = json.loads(rv.data)
    self.assertEqual("user1", res["name"])
    self.assertEqual([1, 99], res["character_list"])



"""
class DemoTestCase(unittest.TestCase):
  def setUp(self):
    # Flask apps testing. See: http://flask.pocoo.org/docs/testing/
    app.config['TESTING'] = True
    app.config['CSRF_ENABLED'] = False
    self.app = app.test_client()
    # Setups app engine test bed. See: http://code.google.com/appengine/docs/python/tools/localunittesting.html#Introducing_the_Python_Testing_Utilities
    self.testbed = testbed.Testbed()
    self.testbed.activate()
    self.testbed.init_datastore_v3_stub()
    self.testbed.init_user_stub()
    self.testbed.init_memcache_stub()

  def tearDown(self):
    self.testbed.deactivate()

  def setCurrentUser(self, email, user_id, is_admin=False):
    os.environ['USER_EMAIL'] = email or ''
    os.environ['USER_ID'] = user_id or ''
    os.environ['USER_IS_ADMIN'] = '1' if is_admin else '0'

  def test_home_redirects(self):
    rv = self.app.get('/')
    assert rv.status == '302 FOUND'

  def test_says_hello(self):
    rv = self.app.get('/hello/world')
    assert 'Hello world' in rv.data

  def test_displays_no_data(self):
    rv = self.app.get('/examples')
    assert 'No examples yet' in rv.data

  def test_inserts_data(self):
    self.setCurrentUser(u'john@example.com', u'123')
    rv = self.app.post('/example/new', data=dict(
      example_name='An example',
      example_description='Description of an example'
    ), follow_redirects=True)
    assert 'Example successfully saved' in rv.data

    rv = self.app.get('/examples')
    assert 'No examples yet' not in rv.data
    assert 'An example' in rv.data

  def test_admin_login(self):
    #Anonymous
    rv = self.app.get('/admin_only')
    assert rv.status == '302 FOUND'
    #Normal user
    self.setCurrentUser(u'john@example.com', u'123')
    rv = self.app.get('/admin_only')
    assert rv.status == '302 FOUND'
    #Admin
    self.setCurrentUser(u'john@example.com', u'123', True)
    rv = self.app.get('/admin_only')
    assert rv.status == '200 OK'

  def test_404(self):
    rv = self.app.get('/missing')
    assert rv.status == '404 NOT FOUND'
    assert '<h1>Not found</h1>' in rv.data
"""

if __name__ == '__main__':
  unittest.main()
