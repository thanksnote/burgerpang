# -*- encoding=utf-8 -*-

__author__ = 'fpgeek'

import urllib
import json
import time
from bs4 import *


teamList = ['A', 'B', 'C', 'D', 'E']


def getPADScheduleJsonDataList():
    jsonDataList = []

    htmlObj = urllib.urlopen('http://m.thisisgame.com/pad/')
    soup = BeautifulSoup(htmlObj)
    divElem = soup.find('div', attrs={'class': 'guerilla-time'})
    tableElem = divElem.find('table')
    tdElems = tableElem.findAll('td')
    for idx, tdElem in enumerate(tdElems):
        scheduleMap = {}
        scheduleMap['team'] = teamList[idx]
        scheduleMap['schedule'] = []
        divElems = tdElem.findAll('div')
        for divElem in divElems:
            className = ''
            if divElem.attrs.has_key('class'):
                className = divElem.attrs.get('class')[0]

            text = divElem.get_text()
            time, kind = getTimeAndKind(text)
            time = filter(str.isdigit, time.encode('utf-8'))
            if time: # 시간이 없는 경우도 발생함 - TODO
                scheduleMap['schedule'].append({'time': time, 'kind_kr': kind, 'kind_en': className})
            else:
                scheduleMap['schedule'].append({'title': text})

        jsonDataList.append(scheduleMap)
    return jsonDataList

def getTimeAndKind(text):
    time = ''
    kind = ''
    if text.find(' ') > -1:
        splitTexts = text.split(' ')
        if len(splitTexts) == 2:
            time, kind = splitTexts
    elif text.find('(') > -1:
        splitTexts = text.split('(')
        if len(splitTexts) == 2 and splitTexts[1].find(')') > -1:
            time = splitTexts[0]
            kind = splitTexts[1].split(')')[0]

    return time, kind

def getPADSchedule():
    jsonDataList = getPADScheduleJsonDataList()
    jsonData = json.dumps(jsonDataList, ensure_ascii=False)
    return jsonData


def getCurrentTimeSchedule():
    jsonDataList = getPADScheduleJsonDataList()
    currentHour = '%d' % time.localtime().tm_hour
    curTimeJsonDatas = filter(lambda d:d.get('time') == currentHour, jsonDataList)
    if len(curTimeJsonDatas) > 0:
        return json.dumps(curTimeJsonDatas[0], ensure_ascii=False)
    return None

if __name__ == '__main__':
    getPADSchedule()