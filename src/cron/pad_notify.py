# -*- encoding=utf-8 -*-

__author__ = 'fpgeek'

import sys
sys.path.append(r'../')

from APNSWrapper import *
from pad.crawler import *

def sendAPNServer(jsonData):
    deviceToken = 'C79E0E9E6F3002C8AD5070C51CDCE09274BCDC89AF4A14DCE0A26E4D94D1857A'

    # create wrapper
    wrapper = APNSNotificationWrapper('cert.pem', True)

    # create message
    message = APNSNotification()
    message.tokenHex(deviceToken)
    print jsonData
    message.appendProperty(APNSProperty('pad_schedule', jsonData.encode('utf-8')))

    # add message to tuple and send it to APNS server
    wrapper.append(message)
    wrapper.notify()

if __name__ == '__main__':
    jsonData = getPADSchedule()
    print jsonData
    # curTimeJsonData = getCurrentTimeSchedule()
    # if curTimeJsonData is not None:
    #     sendAPNServer(curTimeJsonData)
